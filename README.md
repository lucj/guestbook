## Purpose

This contains a simple Elastic Stack distributed a Helm chart in the DockerHub:
[https://hub.docker.com/r/lucj/elastic](https://hub.docker.com/r/lucj/elastic)

## Installation via Argo-CD

```
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: elastic
  namespace: argocd
  finalizers:
    - resources-finalizer.argocd.argoproj.io
spec:
  project: default
  source:
    chart: elastic
    repoURL: registry-1.docker.io/lucj
    targetRevision: v1.0.0
    helm:
      releaseName: elastic
  destination:
    server: https://kubernetes.default.svc
    namespace: elastic
  syncPolicy:
    automated: {}
    syncOptions:
      - CreateNamespace=true
```

## Installation with Helm

```
helm repo add lj_elastic oci://docker.io/lucj/elastic
(????)

helm upgrade --install ...
```

## Installation with Hemlfile

```
```